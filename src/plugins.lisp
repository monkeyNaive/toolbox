(defpackage toolbox
  (:use :cl))
(in-package :toolbox)

;;; 安装插件
(defun setup-plugin (name value)
  (let ((key (symbol-trans name)))
    (setf (gethash key *plugins*) value)))

;;; 加载插件并调用MAIN方法
;;; 所有的插件都应提供一个交互
;;; MAIN函数都应该无参数
;;; 退出统一使用 :exit，并在MAIN函数中return，而不能执行 (exit)
;;; 交互都应该是 [插件名> ] 的格式 
(defun load-plugin (name)
  (let* ((key (symbol-trans name))
	 (path (gethash key *plugins*)))
    (load path)
    (ql:quickload key)
    (funcall (find-symbol "MAIN" (intern (symbol-name key))))))
