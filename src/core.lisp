(defpackage toolbox
  (:use :cl))
(in-package :toolbox)

;; 初始化hash表
(defvar *plugins* (make-hash-table))

;;; 读取用户输入
(defun read-input ()
  (format *query-io* "> ")
  (force-output *query-io*)
  (read-line *query-io*))

;;; 将字符串转换为关键词
(defun symbol-trans (str)
  (intern (string-upcase (string-left-trim ":" str)) :keyword))

;;; 将用户输入进行拆分
(defun split-input (input)
  (let* ((trim-input (string-left-trim " " input))
	 (idx (search " " trim-input)))
    (if (equal idx NIL)
	(list trim-input)
	(list (subseq trim-input 0 idx) (string-trim " " (subseq trim-input (+ 1 idx)))))))

(defun main ()
  (loop
    (let* ((input (read-input))
	  (sinput (split-input input)))
      (cond
	;; 如果用户输入 :exit 则退出进程
	((string-equal input ":exit") (sb-ext:exit))
	;; 拆分用户输入，如果第一项为 :load-plugin 则后续内容视为插件名称
	((string-equal (first sinput) ":load-plugin")
	 (if (gethash (symbol-trans (second sinput)) *plugins*)
	     (load-plugin (second (split-input input)))
	     (block nil (format t "~a~%" "Plugin Not Found")
		    (sb-ext:exit))))
	(t (format t "~a~%" input))))))
