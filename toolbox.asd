(in-package :asdf-user)

(defsystem "toolbox"
  :version "0.1.0"
  :author ""
  :license ""
  :depends-on ("com.inuoe.jzon" "dexador")
  :components ((:module "src"
                :components
                ((:file "core")
		 (:file "plugins"))))
  :description ""
  :build-operation "program-op" ;; leave as is
  :build-pathname "toolbox"
  :entry-point "toolbox:main")
